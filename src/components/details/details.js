import React from 'react';
import Spinner from '../spinner/spinner';
import './details.scss';

const Details = (props) => {
  if(props.character) {
    return (
        <section>
          <div className="image-wrapper">
            <img src={props.character.image} alt={props.character.name} />
          </div>
        
          <ul>
            <li>
              <strong>Name: </strong> 
              {props.character.name}
            </li>
            <li>
              <strong>Location: </strong> 
              {props.character.location.name}
            </li>
            <li>
              <strong>Species: </strong> 
              {props.character.species}
            </li>
            <li>
              <strong>Gender: </strong> 
              {props.character.gender}
            </li>
          </ul>
        </section>
    );
  } else {
    return <Spinner />;
  }
}

export default Details;