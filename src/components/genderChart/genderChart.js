import React from 'react';
import {Bar} from 'react-chartjs-2';

class GenderChart extends React.Component {
  constructor(props) {
    super(props);
    this.options = {
      legend: {
         display: false
      },
      scales: {
        xAxes: [{
            gridLines: {
                color: "rgba(0, 0, 0, 0)",
            }
        }],
        yAxes: [{
            gridLines: {
                color: "rgba(0, 0, 0, 0)",
            },
            ticks: {
              display: false
          }   
        }]
      }
    }
  }

  render() {
    if (this.props.data) {
      return (
        <section className="gender-chart">
          <Bar 
            data={this.props.data}
            options={this.options}  
          />
        </section>
      );
    } else {
      return null;
    }
  }
}

export default GenderChart;