import React from 'react';
import ReactDOM from 'react-dom';
import GenderChart from './genderChart';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<GenderChart />, div);
  ReactDOM.unmountComponentAtNode(div);
});