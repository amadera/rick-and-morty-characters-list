import React from 'react';
import mainService from '../../services/mainService';
import CharactersList from '../characterList/charactersList';
import Details from '../details/details';
import GenderChart from '../genderChart/genderChart';
import Header from '../header/header'
import './main.scss';

class Main extends React.Component {
  constructor() {
    super();
    this.state = {
      'characters': [],
      'selectedCharacter': null,
      'chartData': null,
      'errorMessage': null,
    }
    this.getList();
  }

  async getList() {
    try {
      const list = await mainService.getCharactersList();
      this.setState({
        'characters': list,
        'selectedCharacter': list[0]
      });
      this.generateChartData(list);  
    } catch(error) {
      this.setState({'errorMessage': error});
    }
  }

  generateChartData(characters) {
    let charactersByGender = {
      'Male': [],
      'Female': [],
      'unknown': []
    }
    characters.forEach(character => {
        charactersByGender[character.gender].push(character);
    });
    const labels = Object.keys(charactersByGender);
    this.setState({
        'chartData': {
          labels: labels,
          datasets: [{
            data: labels.map(label => charactersByGender[label].length),
            backgroundColor: [
            'rgba(220, 244, 237, 1)',
            'rgba(255, 255, 204, 1)',
            'rgba(245, 164, 137, 1)',
            ]
          }],
      }
    }); 
  }

  handleSelectedCharacter(character) {
    this.setState({'selectedCharacter': character});
  }

  render() {
    if (!this.state.errorMessage) {
      return (
        <div>
          <Header />
          <main className="main-container">
            <CharactersList 
              characters={this.state.characters} 
              selectedCharacter={this.state.selectedCharacter}
              onClick={this.handleSelectedCharacter.bind(this)} />
            <article className="details-container">
              <Details character={this.state.selectedCharacter}/>
              <GenderChart data={this.state.chartData}/>
            </article>
          </main>
        </div>
      );
    } else {
      return (
        <div>
          <Header />
          <div className="error-message">{this.state.errorMessage}</div>
        </div>
      );
    }
  };
}

export default Main;