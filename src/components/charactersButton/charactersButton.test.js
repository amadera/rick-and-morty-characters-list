import React from 'react';
import ReactDOM from 'react-dom';
import CharactersButton from './characterButton';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<CharactersButton />, div);
  ReactDOM.unmountComponentAtNode(div);
});