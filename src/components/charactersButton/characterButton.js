import React from 'react';
import '../charactersButton/characterButton.scss';

const CharactersButton = (props) => {
  return (
    <li key={props.id}>
      <button 
        className={props.isSelected ? 'character-btn is-selected' : 'character-btn'} 
        onClick={props.onClick}>
        <img 
          src={props.img}
          alt={props.alt}
        ></img>
      </button>
    </li>
  );
}

export default CharactersButton;