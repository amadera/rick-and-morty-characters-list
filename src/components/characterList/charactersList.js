import React from 'react';
import CharactersButton from '../charactersButton/characterButton';
import './charactersList.scss';

class CharactersList extends React.Component {
  renderCharacterCard(character) {
    return <CharactersButton 
              id={character.id}
              img={character.image}
              alt={character.name}
              isSelected={character.id === this.props.selectedCharacter.id}
              onClick={() => this.props.onClick(character)}  
            />;
  }

  render() {
    return (
      <section className="characters-list-container">
        <ul className="characters-list">{this.props.characters.map(character => this.renderCharacterCard(character))}</ul>
      </section>
    );
  };
}

export default CharactersList;