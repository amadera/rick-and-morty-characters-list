import React from 'react';
import ReactDOM from 'react-dom';
import CharactersList from './charactersList';

const characters = [];

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<CharactersList characters={characters}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});