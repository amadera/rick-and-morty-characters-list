import React from 'react';
import '../header/header.scss';

const Header = () => {
  return (
    <header>
      <ul>
        <li className="logo"></li>
        <li>Rick and Morty · Characters List</li>
      </ul>
    </header>
  );
}

export default Header;