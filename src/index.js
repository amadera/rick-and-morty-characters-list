import React from 'react';
import ReactDOM from 'react-dom';
import Main from './components/main/main';

function App() {
  return (
    <div className="App">
      <Main />
    </div>
  );
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);

export default App;