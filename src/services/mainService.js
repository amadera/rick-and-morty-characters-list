import axios from 'axios';

axios.defaults.baseURL = 'https://rickandmortyapi.com/api';
const errorMessage = 'Sorry, there was an error. Please, try again later.';

export default {
  getCharactersList() {
    return axios.get('/character')
      .then(res => res.data.results)
      .catch(error => {
        if (error.response) {
          console.log(error.response);
          return Promise.reject(errorMessage);
        } else if (error.request) {
          console.log(error.request);
          return Promise.reject(errorMessage);
        } else {
          console.log(error.message);
          return Promise.reject(errorMessage);
        }
      })
  }
}