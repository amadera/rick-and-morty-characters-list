This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify


## Technologies

### Axios

I have chosen axios.js to make http request over fetch because fetch it not fully transpiled by babel and need and extra polyfill to keep compatibility with older browsers and axios avoid the middle step of converting the response to .json().

### Chartjs

Although my first thought was to us D3js to do the bar chart, finally I decided to work with Chartjs, since I think for this particular case of a simple bar chart D3js complexity is not necessary.

### SASS

After knowing that working with sass was an option, I decided to use the preprocessor. In this case I didn't take advantage of all sass options because it was not needed, but since the installation is so easy it's worth it just for the use of the nested syntax, in my opinion.

### Create React App

I decided to use Create React App for the setup of the project becuase it's supported by react official documentation and the default configuration provided is good enough in my opinion for this specific project.



## Tasks (Done / To be improved)

  - Single-page responsive web app / The app layout is adapting to different screen sizes, but I think with more time can be improved, mainly to avoid extra white space in some specific sizes with better use of the available space.

  - Grid list with the image of the character. In small screens, it's displayed in a row with a scroll bar to navigate. 
    Two states:
      -> Hover with a scale transition and a shadow
      -> Selected with a border color to highlight  

  - Detail for the selected character with the image and the characteristics.

  - Chart bar with the gender repartition of the current list of characters.

  - Tests / I added some really basic tests, but I would definitely like to improve this part with more tests for the logic.
